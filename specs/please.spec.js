const Please = require('../src/index.js');

describe('Please', () => {
  it('will return a proper please Object', () => {
    // Not much here yet, but placeholder to extend
    expect(new Please()).toMatchObject({});
  });
});

describe('learn', () => {
  let please;

  beforeEach(() => {
    please = new Please();
  });

  it('will assign a new method to the main object', () => {
    please.learn('newMethod', () => {});
    expect(typeof please.newMethod).toBe('function');
  });

  it('will make sure to not override a method', () => {
    please.learn('newMethod', () => {});
    expect(() => please.learn('newMethod', () => {})).toThrow(
      new Error('Method newMethod is already defined')
    );
  });

  it('allows to properly execute learned functions', () => {
    // Works with 1 parameter
    please.learn('add2', (x) => x + 2);
    expect(please.add2(5)).toBe(7);

    // Works with multiples parameters
    please.learn('combine', (...strs) => strs.join(' '));
    expect(please.combine('Hello', 'from', 'jest')).toBe('Hello from jest');
  });
});

describe('methods prop', () => {
  it('returns an Array of available methods', () => {
    const please = new Please();
    expect(please.methods).toEqual([]);
    please['foo'] = () => {};
    expect(please.methods).toEqual(['foo']);
  });

  it('also works with learn', () => {
    const please = new Please();
    please.learn('someMethod', () => {});
    expect(please.methods).toEqual(['someMethod']);
  });
});

describe('freeze', () => {
  test('will freeze the instance', () => {
    const please = new Please();
    expect(Object.isFrozen(please)).toBe(false);
    please.freeze();
    expect(Object.isFrozen(please)).toBe(true);
  });
});
