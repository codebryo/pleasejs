class Please {
  constructor() {}

  get methods() {
    return Object.keys(this);
  }

  learn(name, fn) {
    if (typeof this[name] !== 'undefined') {
      throw new Error(`Method ${name} is already defined`);
    }
    if (typeof fn !== 'function') {
      throw new Error(
        'Make sure to pass a callback function as second parameter'
      );
    }

    this[name] = function (...params) {
      return fn(...params);
    };
  }

  freeze() {
    Object.freeze(this);
  }
}

module.exports = Please;
